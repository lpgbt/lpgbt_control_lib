#!/usr/bin/env python3
###############################################################################
#                                                                             #
#  Copyright (C) 2018-2025 lpGBT Team, CERN                                   #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
"""Driver implementation for lpGBTv2 chip revision"""
# pylint: disable=too-few-public-methods,too-many-ancestors

from .lpgbt import Lpgbt, lpgbt_accessor
from .lpgbt_base_v1v2 import LpgbtBaseV1V2
from .lpgbt_register_map_v2 import LpgbtRegisterMapV2
from .lpgbt_enums_v2 import LpgbtEnumsV2


class LpgbtV2(LpgbtRegisterMapV2, LpgbtEnumsV2, LpgbtBaseV1V2, Lpgbt):
    """Implementation of lpGBT driver for lpGBTv2"""

    @lpgbt_accessor
    def equalizer_setup(self, attenuation=0, cap=0, res0=0, res1=0, res2=0, res3=0):
        """Configures the downlink equalizer

        Arguments:
            attenuation: input attentuator control
            cap: equalizer capacitor value
            res0: equalizer resistor 0 value
            res1: equalizer resistor 1 value
            res2: equalizer resistor 2 value
            res3: equalizer resistor 3 value
        """
        # pylint: disable=too-many-arguments
        # The only reason this method is overloaded is to change the default value
        # for the attenuation

        Lpgbt.equalizer_setup(
            self=self,
            attenuation=attenuation,
            cap=cap,
            res0=res0,
            res1=res1,
            res2=res2,
            res3=res3,
        )

    def clk_tree_disable(
        self, clk_a_disable=False, clk_b_disable=False, clk_c_disable=False
    ):
        """Clock disable feature is not avaliable in lpGBTv2"""

        raise NotImplementedError()
