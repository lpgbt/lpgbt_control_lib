#!/usr/bin/env python3
###############################################################################
#                                                                             #
#  Copyright (C) 2018-2025 lpGBT Team, CERN                                   #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
"""lpGBTv1 constants"""

from enum import IntEnum, unique  # pylint: disable=unused-import
from .lpgbt_enums_base_v1v2 import LpgbtEnumsBaseV1V2


class LpgbtEnumsV1(LpgbtEnumsBaseV1V2):
    """Class containing any lpGBTv1-related constants"""

    ROM_VALUE = 0xA6
