#!/usr/bin/env python3
###############################################################################
#                                                                             #
#  Copyright (C) 2023 lpGBT Team, CERN                                        #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
"""lpGBT driver with calibration methods for analog peripherals"""
# pylint: disable=too-many-lines, too-many-branches, abstract-method

import os
import time
import csv
import numpy as np
from .lpgbt_v1 import LpgbtV1
from .lpgbt_exceptions import LpgbtCalibrationError, LpgbtOutOfRangeError

# pylint: disable=too-many-ancestors
class LpgbtCalibrated(LpgbtV1):
    """Implementation of calibration methods for analog peripherals of lpGBTv1"""

    # pylint: disable=too-many-public-methods
    def __init__(self, logger):
        super().__init__(logger)

        # Version of the data set
        self.calibration_db_version = "1"

        # The calibration values in the dictionary below correspond to the average value
        # of calibration coefficients (obtained for all chips) and will be used if
        # no per-chip calibration data is loaded. In order to improve the quality
        # of calibration the user is expected to call load_calibration_data method.
        self.calibration = {
            "VREF_SLOPE" : -3.3638e-01,
            "VREF_OFFSET" : 1.3426e+02,
            "VDAC_SLOPE" : 4.0906e+03,
            "VDAC_OFFSET" : 2.5420e-01,
            "VDAC_SLOPE_TEMP" : 1.0492e-01,
            "VDAC_OFFSET_TEMP" : -3.8617e-02,
            "ADC_X2_SLOPE" : 1.0429e-03,
            "ADC_X2_OFFSET" : -3.3531e-02,
            "ADC_X2_SLOPE_TEMP" : -2.9308e-09,
            "ADC_X2_OFFSET_TEMP" : 1.2004e-06,
            "ADC_X8_SLOPE" : 3.0928e-04,
            "ADC_X8_OFFSET" : 3.4134e-01,
            "ADC_X8_SLOPE_TEMP" : 6.6980e-10,
            "ADC_X8_OFFSET_TEMP" : -7.6619e-07,
            "ADC_X16_SLOPE" : 1.4331e-04,
            "ADC_X16_OFFSET" : 4.2615e-01,
            "ADC_X16_SLOPE_TEMP" : 1.5782e-09,
            "ADC_X16_OFFSET_TEMP" : -1.3777e-06,
            "VDDMON_SLOPE" : 2.3257e+00,
            "VDDMON_SLOPE_TEMP" : 6.5729e-05,
            "CDAC0_SLOPE" : 2.7212e+05,
            "CDAC0_OFFSET" : -1.3486e-02,
            "CDAC0_R0" : 2.5227e+06,
            "CDAC0_SLOPE_TEMP" : -1.5463e+02,
            "CDAC0_OFFSET_TEMP" : -4.8661e-04,
            "CDAC0_R0_TEMP" : 9.5716e+03,
            "CDAC1_SLOPE" : 2.7223e+05,
            "CDAC1_OFFSET" : -1.3486e-02,
            "CDAC1_R0" : 2.5227e+06,
            "CDAC1_SLOPE_TEMP" : -1.5643e+02,
            "CDAC1_OFFSET_TEMP" : -4.8661e-04,
            "CDAC1_R0_TEMP" : 9.5716e+03,
            "CDAC2_SLOPE" : 2.7227e+05,
            "CDAC2_OFFSET" : -1.3486e-02,
            "CDAC2_R0" : 2.5227e+06,
            "CDAC2_SLOPE_TEMP" : -1.5756e+02,
            "CDAC2_OFFSET_TEMP" : -4.8661e-04,
            "CDAC2_R0_TEMP" : 9.5716e+03,
            "CDAC3_SLOPE" : 2.7193e+05,
            "CDAC3_OFFSET" : -1.3486e-02,
            "CDAC3_R0" : 2.5227e+06,
            "CDAC3_SLOPE_TEMP" : -1.5537e+02,
            "CDAC3_OFFSET_TEMP" : -4.8661e-04,
            "CDAC3_R0_TEMP" : 9.5716e+03,
            "CDAC4_SLOPE" : 2.7277e+05,
            "CDAC4_OFFSET" : -1.3486e-02,
            "CDAC4_R0" : 2.5227e+06,
            "CDAC4_SLOPE_TEMP" : -1.6278e+02,
            "CDAC4_OFFSET_TEMP" : -4.8661e-04,
            "CDAC4_R0_TEMP" : 9.5716e+03,
            "CDAC5_SLOPE" : 2.7295e+05,
            "CDAC5_OFFSET" : -1.3486e-02,
            "CDAC5_R0" : 2.5227e+06,
            "CDAC5_SLOPE_TEMP" : -1.6169e+02,
            "CDAC5_OFFSET_TEMP" : -4.8661e-04,
            "CDAC5_R0_TEMP" : 9.5716e+03,
            "CDAC6_SLOPE" : 2.7342e+05,
            "CDAC6_OFFSET" : -1.3486e-02,
            "CDAC6_R0" : 2.5227e+06,
            "CDAC6_SLOPE_TEMP" : -1.6537e+02,
            "CDAC6_OFFSET_TEMP" : -4.8661e-04,
            "CDAC6_R0_TEMP" : 9.5716e+03,
            "CDAC7_SLOPE" : 2.7328e+05,
            "CDAC7_OFFSET" : -1.3486e-02,
            "CDAC7_R0" : 2.5227e+06,
            "CDAC7_SLOPE_TEMP" : -1.6303e+02,
            "CDAC7_OFFSET_TEMP" : -4.8661e-04,
            "CDAC7_R0_TEMP" : 9.5716e+03,
            "TEMPERATURE_SLOPE" : 4.1320e+02,
            "TEMPERATURE_OFFSET" : -1.8545e+02,
            "TEMPERATURE_UNCALVREF_SLOPE" : 4.5960e-01,
            "TEMPERATURE_UNCALVREF_OFFSET" : -2.1253e+02,
        }

        # Set the junction temperature. It should be updated by the user based on:
        # - thermal simulations and measurements performed in the final system, or
        # - data of its internal temperature sensor obtained during production testing
        #   (estimate_temperature_uncalib_vref)
        self.set_temperature(0.0)


    def load_calibration_data(self, fname="lpgbt_calibration.csv", chipid=""):
        """Load calibration data from a local CSV file based for the specific chipid.
        If no chipid is provided, the chipid will be readout from fuses.

        Arguments:
            fname: Path of CSV file containing calibration data.
            chipid: ChipID for which calibration data should be loaded. If not provided,
                    the chipid will be readout from fuses.

        Raises:
            LpgbtCalibrationError: If loading calibration data failed.
            FileNotFoundError: If the file does not exis
        """

        if not chipid:
            chipid = self.get_chipid_fuses()["chipid"]

        self.logger.info(
            "Loading calibration data for '%s' chip from '%s'", chipid, fname
        )

        calibration_loaded = False

        if not os.path.isfile(fname):
            raise FileNotFoundError

        # pylint: disable=line-too-long
        with open(fname, "r", encoding='ascii') as file:
            reader = csv.reader(file)
            headers = None
            for row in reader:
                if not row:
                    continue
                if row[0].startswith('#'):
                    if "Data Version" in row[0]:
                        version = row[0].split()[-1]
                        if version!=self.calibration_db_version:
                            msg = f"Calibration data version ({version}) not compatible with expected version ({self.calibration_db_version}). Update calibration data or lpgbt_control_lib accordingly."
                            self.logger.error(msg)
                            raise LpgbtCalibrationError(msg)
                elif headers is None:
                    headers = row
                else:
                    if row[0] == chipid:
                        self.calibration = {header: float(value) for header, value in zip(headers[1:], row[1:])}
                        calibration_loaded = True
                        break

        if not calibration_loaded:
            msg = f"Calibration data not available for the {chipid} chip."
            self.logger.error(msg)
            raise LpgbtCalibrationError(msg)


    def set_temperature(self, temperature_c):
        """Set junction temperature.

        Arguments:
            temperature_c: Estimate of junction temperature [C]
        """
        self.temperature_c = temperature_c


    def tune_vref(self, enable=True):
        """Calculate the optimum VREFTUNE code based on the junction temperature
           and apply the setting to the chip. Prior calling this method, the user
           is expected to set the temperature estimate (set_temperature) first if it
           is know or to to use estimate_temperature_uncalib_vref in order to
           estimate it automatically. In the later case, it is advice to use
           auto_tune_vref method instead.

        Arguments:
            enable: Enable VREF generator
        """

        code_opt = round(
            self.calibration["VREF_SLOPE"] * self.temperature_c
            + self.calibration["VREF_OFFSET"]
        )
        self.logger.debug("VREFTune = %d", code_opt)
        self.vref_enable(enable, code_opt)


    def estimate_temperature_uncalib_vref(self, reset_temp_sensor=True):
        """ Estimate temperature using internal temperature sensor and uncalibrated VREF.

        WARNING: this routine WILL NOT WORK for irradiated chips (TID>0)

        Side effects:
            ADC configuration.

        Arguments:
            reset_temp_sensor: Reset temperature sensor before using it.

        Return:
            Temperature estimate in degree C.
        """

        vref_code = round(self.calibration["VREF_OFFSET"])
        self.logger.debug("Enable VREF at code: %d [LSB]", vref_code)
        self.vref_enable(True, vref_code)
        if reset_temp_sensor:
            self.reset_temperature_sensor()

        self.adc_config(
            inp=self.AdcInputSelect.TEMP,
            inn=self.AdcInputSelect.VREF2,
            gain=self.AdcGainSelect.X2,
        )
        time.sleep(0.001)  # wait for 1ms to enable voltage stabilization
        adc_val = np.mean(self.adc_convert(samples=4))  # average 4 ADC readings
        self.logger.debug("Temperature readout: %.1f [LSB]", adc_val)

        # estimate the junction temperature
        temperature_c = (
            adc_val * self.calibration["TEMPERATURE_UNCALVREF_SLOPE"]
            + self.calibration["TEMPERATURE_UNCALVREF_OFFSET"]
        )
        self.logger.debug("Temperature estimate: %.1f [C]", temperature_c)
        return temperature_c


    def auto_tune_vref(self, reset_temp_sensor=True):
        """Auto tune VREF based on the internal temperature sensor.

        WARNING: this routine WILL NOT WORK for irradiated chips (TID>0)

        Side effects:
            ADC configuration.
            Junction temperature.

        Arguments:
            reset_temp_sensor: Reset temperature sensor before using it.
        """
        temperature_c = self.estimate_temperature_uncalib_vref(reset_temp_sensor=reset_temp_sensor)

        # update temperature estimate
        self.set_temperature(temperature_c)

        # tune VREF
        self.tune_vref()


    def vdac_set_vout(self, voltage_v, enable=True):
        """Set requested voltage at the output of the lpGBT voltage DAC.

        Prerequisites:
            VREF should be tuned to 1V.

        Arguments:
            voltage_v: voltage in volts (0-1)
            enable: voltage DAC state

        Raises:
            LpgbtOutOfRangeError: If the requested voltage cannot be achieved.
        """

        assert 0.0 <= voltage_v < 1.0, "Invalid voltage for VDAC"

        dac_code = round(
            (
                self.calibration["VDAC_SLOPE"]
                + self.temperature_c * self.calibration["VDAC_SLOPE_TEMP"]
            )
            * voltage_v
            + self.calibration["VDAC_OFFSET"]
            + self.temperature_c * self.calibration["VDAC_OFFSET_TEMP"]
        )

        if not 0 <= dac_code < self.VDAC_MAX:
            raise LpgbtOutOfRangeError("VDAC can not deliver requested voltage.")

        self.vdac_setup(dac_code, enable)


    def adc_get_vin(self, samples=1):
        """Get input voltage.

        Prerequisites:
            VREF should be tuned to 1V.

        Arguments:
            samples: how many conversions to perform

        Returns:
            Calibrated voltage reading. In case samples is greater than one,
            returns a list of all conversion results.

        Raises:
            LpgbtException: in case the conversion timeout is exceeded
        """

        results = self.adc_convert(samples=samples)
        if not isinstance(results, list):
            results = [results, ]

        gain = self.read_reg(self.ADCCONFIG) & self.ADCCONFIG.ADCGAINSELECT.bit_mask
        gain_str = self.AdcGainSelect(gain).name
        calibrated_results = []
        adc_str = f"ADC_{gain_str}"

        for raw in results:
            cal_res = (
                (
                    self.calibration[f"{adc_str}_SLOPE"]
                    + self.temperature_c * self.calibration[f"{adc_str}_SLOPE_TEMP"]
                )
                * raw
                + self.calibration[f"{adc_str}_OFFSET"]
                + self.temperature_c * self.calibration[f"{adc_str}_OFFSET_TEMP"]
            )
            calibrated_results.append(cal_res)

        if len(calibrated_results) == 1:
            return calibrated_results[0]

        return calibrated_results


    def _cdac_code_to_current(self, chn, code):
        """Return estimate of the CDAC current for specific code.

        Arguments:
            chn: CDAC channel
            code: CDAC code

        Returns:
            Estimate of the output current in Amps.
        """

        assert 0 <= code < 256, "Invalid CDAC code"
        assert 0 <= chn < 8, "Invalid CDAC channel"

        return (
            code
            - self.calibration[f"CDAC{chn}_OFFSET"]
            - self.temperature_c * self.calibration[f"CDAC{chn}_OFFSET_TEMP"]
        ) / (
            self.calibration[f"CDAC{chn}_SLOPE"]
            + self.temperature_c * self.calibration[f"CDAC{chn}_SLOPE_TEMP"]
        )


    def _cdac_code_to_rout(self, chn, code):
        """Return estimate of the CDAC output resistance for specific code.

        Arguments:
            chn: CDAC channel
            code: CDAC code

        Returns:
            Estimate of the output resistance in Ohms.
        """

        assert 0 <= code < 256, "Invalid CDAC code"
        assert 0 <= chn < 8, "Invalid CDAC channel"

        # The rout for code zero is very large and not properly modeled.
        # Return the rout estimate for code 1 instead.
        code = max((1, code))

        # pylint: disable=invalid-name
        r0 = (self.calibration[f"CDAC{chn}_R0"]
              + self.temperature_c * self.calibration[f"CDAC{chn}_R0_TEMP"]
             )
        return  r0 / code


    def _cdac_get_optimum_code_for_current(self, chn, current_a):
        """Return optimum CDAC code for the requested current (in amps)

        Arguments:
            chn: CDAC channel
            current_a: Output current [A]

        Returns:
            The optimum CDAC code

        Raises:
            LpgbtOutOfRangeError: If the requested current cannot be achieved.
        """

        assert 0 <= current_a < 1e-3, "Invalid CDAC current"
        assert 0 <= chn < 8, "Invalid CDAC channel"

        code = round(
            (
                self.calibration[f"CDAC{chn}_SLOPE"]
                + self.temperature_c * self.calibration[f"CDAC{chn}_SLOPE_TEMP"]
            )
            * current_a
            + self.calibration[f"CDAC{chn}_OFFSET"]
            + self.temperature_c * self.calibration[f"CDAC{chn}_OFFSET_TEMP"]
        )

        if not 0 <= code < 256:
            raise LpgbtOutOfRangeError("CDAC can not deliver requested current.")

        return code


    def cdac_set_current(self, chn, current_a, enable=True):
        """Configure the lpGBT current DAC.

        Side effects:
            Disable all other current sources.

        Arguments:
            chn: ADC channel to connect to current DAC to
            current_a: Output current [A]
            enable: Current DAC state
        """

        assert 0 <= current_a < 1e-3, "Invalid CDAC current"
        assert 0 <= chn < 8, "Invalid CDAC channel"

        code = self._cdac_get_optimum_code_for_current(chn=chn, current_a=current_a)
        self.cdac_setup(code=code, chns=1<<chn, enable=enable)


    def measure_resistance(self, chn=0, expected_r_ohm=None, improve_precision=True,
        disable_cdac_after_measurement=True
    ):
        """Measure resistance connected between the ground (VSS) and a given ADC channel.
           If the expected_r_ohm is provided (not None), it will be used to set the current source
           (CDAC) in order to obtain optimum voltage drop across the resistors (around 0.5 Vref).
           Alternatively, if expected_r_ohm is not provided (None), an auto ranging procedure will
           be executed in order to estimate the value of the resistor first.

           When measuring resistive temperature sensors (e.g. PT1000 or NTC) it is recommended
           to set expected_r_ohm in order to prevent auto ranging as it will minimize the error
           introduced by integral non-linearity errors of the current source and should result
           in less "noisy" results.

           In order to improve the measurement precision, one could set improve_precision
           to true. It will result in the method performing several measurements for different
           CDAC codes in order to average out the errors introduced by the CDAC non-linearity.
           The number of measurements depends on the resistance value and it is set automatically
           for maximum precision.

           After the resistance measurement is performed, the current DAC will be disabled if
           disable_cdac_after_measurement flag is set.

        Prerequisites:
            VREF should be tuned to 1V.

        Side effects:
            ADC settings.
            CDAC settings.

        Arguments:
            chn: ADC channel to be used for the measurement
            expected_r_ohm: Expected resistance [Ohms]. If not set, auto ranging will be done
                            automatically
            improve_precision: Improve precision by preforming multiple measurements
            disable_cdac_after_measurement: Disable CDAC after measurement

        Returns:
            Resistance [Ohms]

        Raises:
            LpgbtException: in case the conversion timeout is exceeded
        """

        chns = 1 << chn
        self.adc_config(
            inp=self.AdcInputSelect.EXT0 + chn,
            inn=self.AdcInputSelect.VREF2,
            gain=self.AdcGainSelect.X2,
        )

        if expected_r_ohm is None:
            self.logger.info("expected_r_ohm not provided. Performing auto ranging.")
            cdac_code = 1
            while True:
                self.cdac_setup(cdac_code, chns=chns, enable=True)
                vadc = np.mean(self.adc_get_vin(samples=1))
                if vadc > self.VREF_NOMINAL / 2 or cdac_code >= 128:
                    break
                cdac_code *= 2
            expected_r_ohm = vadc / self._cdac_code_to_current(chn=chn, code=cdac_code)
            self.logger.debug("First estimate of resistance: %.2f kOhm", expected_r_ohm/1e3)

        current_a = self.VREF_NOMINAL / 2 / expected_r_ohm
        cdac_code = self._cdac_get_optimum_code_for_current(chn=chn, current_a=current_a)
        self.logger.debug("Optimum cdac code: %d", cdac_code)
        cdac_codes = [
            cdac_code,
        ]
        if improve_precision:
            cdac_codes = list(range(int(cdac_code * 0.9), int(cdac_code * 1.1)))
        rloads = []
        for cdac_code in cdac_codes:
            self.cdac_setup(cdac_code, chns=chns, enable=True)
            iout = self._cdac_code_to_current(chn=chn, code=cdac_code)
            rout = self._cdac_code_to_rout(chn=chn, code=cdac_code)
            vadc = np.mean(self.adc_get_vin(samples=10))
            rmeas = vadc / iout
            self.logger.debug("VADC: %.3f V", vadc)
            if vadc < 0.25:
                self.logger.warning("Initial estimate of the resistance was too high")
            if vadc > 0.75:
                self.logger.warning("Initial estimate of the resistance was too low")
            rload = rmeas / (1 - rmeas / rout)
            self.logger.debug(
                "CODE: %3d IOUT: %.3f [mA] ROUT: %6.3f [kOhm] VADC: %.3f [V] LOAD: %.1f [Ohm]",
                cdac_code,
                1e3 * iout,
                rout * 1e-3,
                vadc,
                rload,
            )
            rloads.append(rload)

        if disable_cdac_after_measurement:
            self.cdac_setup(0, chns=0, enable=False)

        return np.mean(rloads)


    def measure_temperature(self, samples=8, reset_temp_sensor=True):
        """Measure junction temperature.

        Prerequisites:
            VREF should be tuned to 1V.

        Side effects:
            ADC settings.

        Arguments:
            samples: Number of ADC samples to average during the measurement
            reset_temp_sensor: Should the temperature sensor be reset prior to its use

        Returns:
            Temperature [C]

        Raises:
            LpgbtException: in case the conversion timeout is exceeded
        """

        if reset_temp_sensor:
            self.reset_temperature_sensor()

        self.adc_config(
            inp=self.AdcInputSelect.TEMP,
            inn=self.AdcInputSelect.VREF2,
            gain=self.AdcGainSelect.X2,
        )
        adc_vals = self.adc_get_vin(samples=samples)
        adc_val = np.mean(adc_vals)
        temp = (adc_val * self.calibration["TEMPERATURE_SLOPE"] +
               self.calibration["TEMPERATURE_OFFSET"])
        return temp


    def _vddmon_enable(self, power_supply, enable=True):
        """Helper function to enable/disable a specific VDD monitor

        Arguments:
            power_supply: Power supply rail to be measured (AdcInputSelect.VDDTX,
                          AdcInputSelect.VDDRX, AdcInputSelect.VDD, AdcInputSelect.VDDA)
            enable: State of the vdd monitor
        """

        adcmon = self.read_reg(self.ADCMON)
        if power_supply == self.AdcInputSelect.VDDTX:
            mask = self.ADCMON.VDDTXMONENA.bit_mask
        elif power_supply == self.AdcInputSelect.VDDRX:
            mask = self.ADCMON.VDDRXMONENA.bit_mask
        elif power_supply == self.AdcInputSelect.VDD:
            mask = self.ADCMON.VDDMONENA.bit_mask
        elif power_supply == self.AdcInputSelect.VDDA:
            mask = self.ADCMON.VDDANMONENA.bit_mask
        else:
            mask = 0

        if enable:
            adcmon |= mask
        else:
            # pylint: disable=invalid-unary-operand-type
            adcmon &= ~mask

        self.write_reg(self.ADCMON, adcmon)


    def measure_power_supply_voltage(self, power_supply=12, samples=1,
        disable_monitor_after_measurement=True
    ):
        """Measure power supply voltage.

        Prerequisites:
            VREF should be tuned to 1V.

        Side effects:
            ADC settings.
            Settings of VDD monitors.

        Arguments:
            power_supply: Power supply rail to be measured (AdcInputSelect.VDDTX,
                          AdcInputSelect.VDDRX, AdcInputSelect.VDD, AdcInputSelect.VDDA)
            samples: how many conversions to perform

        Returns:
            Calibrated reading of a power supply voltage in V

        Raises:
            LpgbtException: in case the conversion timeout is exceeded
        """

        assert power_supply in (
            self.AdcInputSelect.VDDTX,
            self.AdcInputSelect.VDDRX,
            self.AdcInputSelect.VDD,
            self.AdcInputSelect.VDDA
        ), "Invalid power_supply."

        # Configure ADC mux
        self.adc_config(
            inp=power_supply,
            inn=self.AdcInputSelect.VREF2,
            gain=self.AdcGainSelect.X2,
        )

        # Enable required VDD monitor
        self._vddmon_enable(power_supply, True)

        # perform conversion
        vadc = self.adc_get_vin(samples)

        vsup = vadc * (self.calibration["VDDMON_SLOPE"] +
                       self.temperature_c * self.calibration["VDDMON_SLOPE_TEMP"]
                      )

        # disable VDD monitor (if requested)
        if disable_monitor_after_measurement:
            self._vddmon_enable(power_supply, False)

        return vsup


    def log_calibration(self):
        """Log calibration coefficients using internal logger."""

        for name, value in self.calibration.items():
            self.logger.info("  %-20s: %.4e", name, value)
