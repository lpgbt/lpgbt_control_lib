#!/usr/bin/env python3
###############################################################################
#                                                                             #
#        _        _____ ____ _______                                          #
#       | |      / ____|  _ \__   __|                                         #
#       | |_ __ | |  __| |_) | | |                                            #
#       | | '_ \| | |_ |  _ <  | |                                            #
#       | | |_) | |__| | |_) | | |                                            #
#       |_| .__/ \_____|____/  |_|                                            #
#         | |                                                                 #
#         |_|                                                                 #
#                                                                             #
#  Copyright (C) 2018-2025 lpGBT Team, CERN                                   #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
""" lpGBTv2 constants"""

# pylint: disable=too-many-lines,too-few-public-methods,missing-class-docstring
# pylint: disable=empty-docstring,line-too-long,invalid-name
# pylint: disable=missing-function-docstring

from .lpgbt_register_map_base_v1v2 import LpgbtRegisterMapBaseV1V2

class LpgbtRegisterMapV2(LpgbtRegisterMapBaseV1V2):
    """Class containing all lpGBTv2-related constants"""

    class CLKGCONFIG1:
        """"""

        address = 0x0021

        @staticmethod
        def __str__():
            return "CLKGConfig1"

        @staticmethod
        def __int__():
            return 0x0021

        class CDRCONTROLOVERRIDEENABLE:
            """Enables the control override of the state machine; default: 0"""

            offset = 7
            length = 1
            bit_mask = 128

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGDISABLEFRAMEALIGNERLOCKCONTROL:
            """Disables the use of the frame aligner's lock status; default: 0"""

            offset = 6
            length = 1
            bit_mask = 64

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGCDRRES:
            """CDR's filter resistor enable; default: 1"""

            offset = 5
            length = 1
            bit_mask = 32

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGVCORAILMODE:
            """VCO rail mode; [0: voltage mode, fixed to VDDRX; 1: current mode, value selectable using CLKGVcoDAC]; default: 1"""

            offset = 4
            length = 1
            bit_mask = 16

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGVCODAC:
            """Current DAC for the VCO [3.76: 0.470 : 7.1] mA. MSB is ignored, but set by default.; default: 8"""

            offset = 0
            length = 4
            bit_mask = 15

            @staticmethod
            def validate(value):
                return value in range(16)


    class EQCONFIG:
        """Main equalizer configuration register"""

        address = 0x0037

        @staticmethod
        def __str__():
            return "EQConfig"

        @staticmethod
        def __int__():
            return 0x0037

        class EQATTENUATION:
            """Attenuation of the equalizer. Use a gain setting of `1/1` (`EQAttenuation[1:0]=2'd0`)
            when VTRX+ is used.

                +-----------------------------------+------------------------------------------+
                | EQAttenuation[1:0]                |  Gain [V/V]                              |
                +===================================+==========================================+
                | 2'd0                              | 1/1                                      |
                +-----------------------------------+------------------------------------------+
                | 2'd1                              | 2/3                                      |
                +-----------------------------------+------------------------------------------+
                | 2'd2                              | 2/3                                      |
                +-----------------------------------+------------------------------------------+
                | 2'd3                              | 1/3                                      |
                +-----------------------------------+------------------------------------------+

            """

            offset = 3
            length = 2
            bit_mask = 24

            @staticmethod
            def validate(value):
                return value in range(4)

        class EQCAP:
            """Capacitance select for the equalizer

            +-----------------------------------+------------------------------------------+
            | EQCap[1:0]                        | Capacitance [fF]                         |
            +===================================+==========================================+
            | 2'd0                              | 0                                        |
            +-----------------------------------+------------------------------------------+
            | 2'd1                              | 70                                       |
            +-----------------------------------+------------------------------------------+
            | 2'd2                              | 70                                       |
            +-----------------------------------+------------------------------------------+
            | 2'd3                              | 140                                      |
            +-----------------------------------+------------------------------------------+

            """

            offset = 0
            length = 2
            bit_mask = 3

            @staticmethod
            def validate(value):
                return value in range(4)


    class CLKTREE:
        """Clock tree disable feature. Not available in lpGBTv2."""

        address = 0x0141

        @staticmethod
        def __str__():
            return "CLKTree"

        @staticmethod
        def __int__():
            return 0x0141

        class CLKTREEMAGICNUMBER:
            """Ignored."""

            offset = 3
            length = 5
            bit_mask = 248

            @staticmethod
            def validate(value):
                return value in range(32)

        class CLKTREECDISABLE:
            """Ignored."""

            offset = 2
            length = 1
            bit_mask = 4

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKTREEBDISABLE:
            """Ignored."""

            offset = 1
            length = 1
            bit_mask = 2

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKTREEADISABLE:
            """Ignored."""

            offset = 0
            length = 1
            bit_mask = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class ROM:
        """Register with fixed (non zero value). Can be used for testing purposes."""

        address = 0x01D7

        @staticmethod
        def __str__():
            return "ROM"

        @staticmethod
        def __int__():
            return 0x01D7

        class ROMREG:
            """All read requests for this register should yield value `0xAE` (as opposed to `0xA5` for lpGBTv0 and `0xA6` for lpGBTv1)."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)
