#!/usr/bin/env python3
###############################################################################
#                                                                             #
#        _        _____ ____ _______                                          #
#       | |      / ____|  _ \__   __|                                         #
#       | |_ __ | |  __| |_) | | |                                            #
#       | | '_ \| | |_ |  _ <  | |                                            #
#       | | |_) | |__| | |_) | | |                                            #
#       |_| .__/ \_____|____/  |_|                                            #
#         | |                                                                 #
#         |_|                                                                 #
#                                                                             #
#  Copyright (C) 2018-2025 lpGBT Team, CERN                                   #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
""" lpGBTv1 constants"""

# pylint: disable=too-many-lines,too-few-public-methods,missing-class-docstring
# pylint: disable=empty-docstring,line-too-long,invalid-name
# pylint: disable=missing-function-docstring

from .lpgbt_register_map_base_v1v2 import LpgbtRegisterMapBaseV1V2

class LpgbtRegisterMapV1(LpgbtRegisterMapBaseV1V2):
    """Class containing all lpGBTv1-related constants"""

    class CLKGCONFIG1:
        """"""

        address = 0x0021

        @staticmethod
        def __str__():
            return "CLKGConfig1"

        @staticmethod
        def __int__():
            return 0x0021

        class CDRCONTROLOVERRIDEENABLE:
            """Enables the control override of the state machine; default: 0"""

            offset = 7
            length = 1
            bit_mask = 128

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGDISABLEFRAMEALIGNERLOCKCONTROL:
            """Disables the use of the frame aligner's lock status; default: 0"""

            offset = 6
            length = 1
            bit_mask = 64

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGCDRRES:
            """CDR's filter resistor enable; default: 1"""

            offset = 5
            length = 1
            bit_mask = 32

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGVCORAILMODE:
            """VCO rail mode; [0: voltage mode, fixed to VDDRX; 1: current mode, value selectable using CLKGVcoDAC]; default: 1"""

            offset = 4
            length = 1
            bit_mask = 16

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKGVCODAC:
            """Current DAC for the VCO [0: 0.470 : 7.1] mA; default: 8"""

            offset = 0
            length = 4
            bit_mask = 15

            @staticmethod
            def validate(value):
                return value in range(16)

    class CLKTREE:
        """Clock tree disable feature. Could be used for TMR testing."""

        address = 0x0141

        @staticmethod
        def __str__():
            return "CLKTree"

        @staticmethod
        def __int__():
            return 0x0141

        class CLKTREEMAGICNUMBER:
            """Has to be set to 5'h15 in order for clock masking (disabling) to be active"""

            offset = 3
            length = 5
            bit_mask = 248

            @staticmethod
            def validate(value):
                return value in range(32)

        class CLKTREECDISABLE:
            """If asserted and ClkTreeMagicNumber set to 5'h15, branch C of clock tree is disabled"""

            offset = 2
            length = 1
            bit_mask = 4

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKTREEBDISABLE:
            """If asserted and ClkTreeMagicNumber set to 5'h15, branch B of clock tree is disabled"""

            offset = 1
            length = 1
            bit_mask = 2

            @staticmethod
            def validate(value):
                return value in range(2)

        class CLKTREEADISABLE:
            """If asserted and ClkTreeMagicNumber set to 5'h15, branch A of clock tree is disabled"""

            offset = 0
            length = 1
            bit_mask = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class ROM:
        """Register with fixed (non zero value). Can be used for testing purposes."""

        address = 0x01D7

        @staticmethod
        def __str__():
            return "ROM"

        @staticmethod
        def __int__():
            return 0x01D7

        class ROMREG:
            """All read requests for this register should yield value `0xA6` (as opposed to `0xA5` for lpGBTv0)."""

            offset = 0
            length = 8
            bit_mask = 255

            @staticmethod
            def validate(value):
                return value in range(256)
