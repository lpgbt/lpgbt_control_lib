#!/usr/bin/env python
"""Bit-wise majority voting functions"""

from collections import Counter


def majority_vote(values):
    """Performs bit-wise majority voting for list of values."""

    result_voted = 0
    mismatches = {}
    for bit in range(32):
        bits = []
        for value in values:
            bit_value = (value >> bit) & 0x1
            bits.append(bit_value)
        bits_counter = Counter(bits)
        bit_voted, _ = bits_counter.most_common(1)[0]
        if len(bits_counter.most_common()) != 1:
            mismatches[bit] = {"voted": bit_voted, "bits": bits}
        result_voted |= bit_voted << bit
    return {"result_voted": result_voted, "mismatches": mismatches}


if __name__ == "__main__":
    assert majority_vote([1, 1, 1]) == {"result_voted": 1, "mismatches": {}}
    assert majority_vote([1, 1, 2]) == {
        "result_voted": 1,
        "mismatches": {
            0: {"voted": 1, "bits": [1, 1, 0]},
            1: {"voted": 0, "bits": [0, 0, 1]},
        },
    }
    assert majority_vote([1, 2, 1]) == {
        "result_voted": 1,
        "mismatches": {
            0: {"voted": 1, "bits": [1, 0, 1]},
            1: {"voted": 0, "bits": [0, 1, 0]},
        },
    }
    assert majority_vote([2, 1, 1]) == {
        "result_voted": 1,
        "mismatches": {
            0: {"voted": 1, "bits": [0, 1, 1]},
            1: {"voted": 0, "bits": [1, 0, 0]},
        },
    }
    assert majority_vote([1, 2, 2]) == {
        "result_voted": 2,
        "mismatches": {
            0: {"voted": 0, "bits": [1, 0, 0]},
            1: {"voted": 1, "bits": [0, 1, 1]},
        },
    }
    assert majority_vote([2, 2, 1]) == {
        "result_voted": 2,
        "mismatches": {
            0: {"voted": 0, "bits": [0, 0, 1]},
            1: {"voted": 1, "bits": [1, 1, 0]},
        },
    }
    assert majority_vote([2, 1, 2]) == {
        "result_voted": 2,
        "mismatches": {
            0: {"voted": 0, "bits": [0, 1, 0]},
            1: {"voted": 1, "bits": [1, 0, 1]},
        },
    }

    assert majority_vote([0x12345678, 0x12345678, 0x12345678]) == {
        "result_voted": 0x12345678,
        "mismatches": {},
    }
    assert (
        majority_vote([0x12345678, 0x12345678, 0x87654321])["result_voted"]
        == 0x12345678
    )

    assert (
        majority_vote([0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF])["result_voted"]
        == 0xFFFFFFFF
    )
    assert (
        majority_vote([0x00000000, 0xFFFFFFFF, 0xFFFFFFFF])["result_voted"]
        == 0xFFFFFFFF
    )
    assert (
        majority_vote([0xFFFFFFFF, 0x00000000, 0xFFFFFFFF])["result_voted"]
        == 0xFFFFFFFF
    )
    assert (
        majority_vote([0xFFFFFFFF, 0xFFFFFFFF, 0x00000000])["result_voted"]
        == 0xFFFFFFFF
    )

    assert (
        majority_vote([0x00000000, 0x00000000, 0x00000000])["result_voted"]
        == 0x00000000
    )
    assert (
        majority_vote([0x00000000, 0x00000000, 0xFFFFFFFF])["result_voted"]
        == 0x00000000
    )
    assert (
        majority_vote([0x00000000, 0xFFFFFFFF, 0x00000000])["result_voted"]
        == 0x00000000
    )
    assert (
        majority_vote([0xFFFFFFFF, 0x00000000, 0x00000000])["result_voted"]
        == 0x00000000
    )
