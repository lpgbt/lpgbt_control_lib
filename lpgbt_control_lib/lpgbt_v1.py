#!/usr/bin/env python3
###############################################################################
#                                                                             #
#  Copyright (C) 2018-2025 lpGBT Team, CERN                                   #
#                                                                             #
#  This IP block is free for HEP experiments and other scientific research    #
#  purposes. Commercial exploitation of a chip containing the IP is not       #
#  permitted.  You can not redistribute the IP without written permission     #
#  from the authors. Any modifications of the IP have to be communicated back #
#  to the authors. The use of the IP should be acknowledged in publications,  #
#  public presentations, user manual, and other documents.                    #
#                                                                             #
#  This IP is distributed in the hope that it will be useful, but WITHOUT ANY #
#  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS  #
#  FOR A PARTICULAR PURPOSE.                                                  #
#                                                                             #
###############################################################################
"""Driver implementation for lpGBTv1 chip revision"""
# pylint: disable=too-few-public-methods,too-many-ancestors

from .lpgbt import Lpgbt
from .lpgbt_base_v1v2 import LpgbtBaseV1V2
from .lpgbt_register_map_v1 import LpgbtRegisterMapV1
from .lpgbt_enums_v1 import LpgbtEnumsV1

class LpgbtV1(LpgbtRegisterMapV1, LpgbtEnumsV1, LpgbtBaseV1V2, Lpgbt):
    """Implementation of lpGBT driver for lpGBTv1"""
